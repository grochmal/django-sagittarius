from django.shortcuts import render, render_to_response, get_object_or_404
from django.views.generic import TemplateView , RedirectView \
                               , DetailView   , ListView \
                               , FormView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseForbidden
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.sites.models import Site

from sagittarius.models import Section, Tag, ArticleEntry, LinkEntry
from sagittarius.forms import SearchForm

class HackErrorPg(TemplateView):
    '''Hack to work around django bug 24829

    See: https://code.djangoproject.com/ticket/24829
    '''
    def dispatch(self, request, *args, **kwargs):
        response = super(HackErrorPg, self).dispatch(request , *args, **kwargs)
        response.render()
        return response

class Error400View(HackErrorPg):
    template_name = 'sagittarius/error/400.html'
class Error403View(HackErrorPg):
    template_name = 'sagittarius/error/403.html'
class Error404View(HackErrorPg):
    template_name = 'sagittarius/error/404.html'
class Error500View(HackErrorPg):
    template_name = 'sagittarius/error/500.html'

class SectionView(ListView):
    template_name       = 'sagittarius/section.html'
    context_object_name = 'article_list'
    paginate_by         = 2

    def get_queryset(self):
        self.section = get_object_or_404(Section, slug=self.kwargs['section'])
        live = ArticleEntry.live.for_site(self.request)
        return live.filter(section=self.section)

    def get_context_data(self, **kwargs):
        context = super(SectionView, self).get_context_data(**kwargs)
        context['section'] = self.section
        return context

class ArticleView(DetailView):
    template_name       = 'sagittarius/article.html'
    context_object_name = 'article'

    def get_object(self):  # otherwise we would need to pass the PK
        live_or_prot = ( ArticleEntry.LIVE_STATUS
                       , ArticleEntry.PROTECTED_STATUS )
        self.site    = Site.objects.get_current(self.request)
        self.section = get_object_or_404(Section, slug=self.kwargs['section'])
        self.article = get_object_or_404( ArticleEntry
                                        , site=self.site
                                        , hash=self.kwargs['hash']
                                        , status__in=live_or_prot
                                        , section=self.section
                                        , slug=self.kwargs['article'] )
        return self.article

class LinkView(RedirectView):
    permanent    = False
    query_string = False

    def get_redirect_url(self, *args, **kwargs):
        link = get_object_or_404( LinkEntry
                                , hash=kwargs['hash'], slug=kwargs['link'] )
        return link.link

class TagView(RedirectView):
    permanent = False
    query_string = False

    def get_redirect_url(self, *args, **kwargs):
        slug  = kwargs['tag']
        tag   = get_object_or_404(Tag, slug=slug)
        query = '?query=%s&search_tag=on' % slug
        return reverse('sagittarius_search') + query

class SearchView(FormView):
    template_name = 'sagittarius/search.html'
    paginate_by   = 2

    def post(self, request, *args, **kwargs):
        return HttpResponseForbidden()

    def put(self, request, *args, **kwargs):
        return HttpResponseForbidden()

    def perform_search(self, query, stitle, sbody, stag, case):
        tags   = ArticleEntry.live.none()
        titles = ArticleEntry.live.none()
        bodies = ArticleEntry.live.none()
        live   = ArticleEntry.live.for_site(self.request)
        if stag and case : tags = live.filter(tags__slug=self.query)
        elif stag        : tags = live.filter(tags__slug__iexact=query)
        if sbody and case : bodies = live.filter(body_html__contains=query)
        elif sbody        : bodies = live.filter(body_html__icontains=query)
        if stitle and case : titles = live.filter(title__contains=query)
        elif stitle        : titles = live.filter(title__icontains=query)
        return (tags | titles | bodies).distinct()

    def paginate_search(self, object_list, mutable_get):
        paginator = Paginator(object_list, self.paginate_by)
        page      = mutable_get.get('page', None)
        try:
            articles = paginator.page(page)
        except PageNotAnInteger:
            articles = paginator.page(1)
        except EmptyPage:
            articles = paginator.page(paginator.num_pages)
        return articles

    def get(self, request, *args, **kwargs):
        self.results = ArticleEntry.live.none()
        self.query   = self.request.GET.get('query'        , None)
        self.stitle  = self.request.GET.get('search_title' , None)
        self.sbody   = self.request.GET.get('search_body'  , None)
        self.stag    = self.request.GET.get('search_tag'   , None)
        # note: case sensitive search does not work in sqlite
        self.case    = self.request.GET.get('search_case'  , None)
        form = SearchForm(self.request.GET or None)
        if form.is_valid():
            self.results = self.perform_search( self.query
                                              , self.stitle , self.sbody
                                              , self.stag   , self.case  )
        self.results = self.paginate_search(self.results, self.request.GET)
        return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['article_list'] = self.results
        context['query']        = self.query
        get_query               = self.request.GET.copy()
        get_query.pop('page', None)
        context['get_query']    = get_query.urlencode()
        return context

class FeedView(ListView):
    template_name       = 'sagittarius/feed.atom'
    context_object_name = 'articles'
    content_type        = 'application/atom+xml'

    def get_queryset(self):
        live = ArticleEntry.live.for_site(self.request)
        return live.all()[:12]

    def get_context_data(self, **kwargs):
        context = super(FeedView, self).get_context_data(**kwargs)
        try:
            last = ArticleEntry.live.for_site(self.request).latest('pub_date')
        except ObjectDoesNotExist:
            raise Http404
        context['last_updated'] = last.pub_date
        return context

class NewsView(ListView):
    template_name       = 'sagittarius/news.html'
    context_object_name = 'article_list'

    def get_queryset(self):
        live = ArticleEntry.live.for_site(self.request)
        return live.order_by('-pub_date')[:6]

