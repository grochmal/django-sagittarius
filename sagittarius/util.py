import markdown, re

def md2html(text):
    return markdown.markdown( text
                            , output_format='xhtml5'
                            , extensions=['markdown.extensions.extra'] )

def make_toc_tree(ls, tree, level):
    while ls:
        s,t,i = ls.pop()
        if s == level:
            tree.append({'title':t , 'id':i , 'sub':None})
        elif '#' == s:
            ls.append((s,t,i))
            sub = []
            make_toc_tree(ls, sub, s)
            tree.append({'title':t , 'id':i , 'sub':sub})
        elif '' == s:
            ls.append((s,t,i))
            return

def print_toc_html(indent, inc, ls):
    html = ''
    for d in ls:
        if not d['sub'] is None:
            html += '%s<li>\n' % indent
            html += '%s  <a href="#%s">%s</a>\n' % (indent,d['id'],d['title'])
            html += '%s  <ul>\n' % indent
            html += print_toc_html(indent+inc, inc, d['sub'])
            html += '%s  </ul>\n' % indent
            html += '%s</li>\n' % indent
        else:
            html += ( '%s<li><a href="#%s">%s</a></li>\n'
                    % (indent,d['id'],d['title'])       )
    return html

def make_content(text):
    r = re.findall('\n\#\#(#)?\s+([^{#]+)\s+\#*\s*{\s*\#([\w-]+)\s*}', text)
    r.reverse()
    tree = []
    make_toc_tree(r, tree, '')
    html  = '  <ol>\n'
    html += print_toc_html('', '    ', tree)
    html += '  </ol>'
    return html

