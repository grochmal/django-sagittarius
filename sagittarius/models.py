from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator

import datetime
from pytz import timezone
from os import path
from hashlib import sha1
from django import forms
from django.utils.text import slugify
from django.core.files.storage import FileSystemStorage
from sagittarius.util import md2html, make_content
from django.conf import settings
try:
    from conf.settings import SAGITTARIUS_MAX_FILE_SIZE
except ImportError:
    SAGITTARIUS_MAX_FILE_SIZE = (33554432 , '32MB')

class SiteContext(models.Model):
    site      = models.OneToOneField( Site
                                    , primary_key=True
                                    , related_name='context' )
    author    = models.CharField(max_length=30)
    copyright = models.CharField( max_length=100
                                , help_text='the copyright attribution' )
    logo      = models.TextField(help_text="html allowed")
    motd      = models.TextField(help_text="message of the day (html)")
    news      = models.TextField(help_text="excerpt for the main page (html)")
    style     = models.TextField( blank=True
                                , help_text="Extra style (CSS) for this site"
                                )

    def __unicode__(self):
        return self.site.name

    def get_absolute_url(self):
        return reverse('sagittarius_news')

    class Meta:
        ordering = ['site']

class Section(models.Model):
    title       = models.CharField( max_length=20
                                  , help_text='Max 20 characters' )
    slug        = models.SlugField( unique=True
                                  , help_text='To build the title' )
    description = models.TextField()
    priority    = models.PositiveSmallIntegerField(default=0)

    def live_articles(self):
        '''Hack to filter articles on status directly'''
        from sagittarius.models import ArticleEntry
        return self.articles.filter(status=ArticleEntry.LIVE_STATUS)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('sagittarius_section', kwargs={'section':self.slug})

    class Meta:
        ordering = ['-priority', 'title']

class Tag(models.Model):
    slug        = models.SlugField( verbose_name='The Tag'
                                  , unique=True, help_text='no spaces')
    description = models.TextField()

    def __unicode__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('sagittarius_tag', kwargs={'tag':self.slug})

    class Meta:
        ordering = ['slug']

hash_validator = RegexValidator( r'^[0-9a-f]*$'
                               , 'Must be a hexadecimal (0-9a-f) value')

class BaseEntry(models.Model):
    slug       = models.SlugField(help_text='To build the URL')
    priority   = models.PositiveSmallIntegerField(default=0)
    featured   = models.BooleanField(default=False)
    hash       = models.CharField( max_length=6, blank=True
                                 , validators=[hash_validator]
                                 , help_text='6 character hash (default sha1)')
    menu_entry = models.CharField( max_length=30
                                 , help_text='Short entry (max 30 chars)' )

    def __unicode__(self):
        return self.menu_entry

    class Meta:
        abstract = True

class LinkManagerAll(models.Manager):
    '''Hack to make links look like articles for navigation menu purposes'''
    use_for_related_fields = True

    def live(self, *args, **kwargs):
        return self.get_queryset()

    def for_site(self, request):
        site = Site.objects.get_current(request)
        return self.live().filter(site=site)

class LinkEntry(BaseEntry):
    section = models.ForeignKey(Section,   related_name='links')
    site    = models.ManyToManyField(Site, related_name='links')
    link    = models.URLField()

    objects = LinkManagerAll()

    def get_absolute_url(self):
        return reverse('sagittarius_link', kwargs={ 'link' : self.slug
                                                  , 'hash' : self.hash })

    def save(self, force_insert=False, force_update=False, using=None):
        if not self.hash:
            self.hash = sha1(self.link).hexdump()[:6]
        super(LinkEntry, self).save(force_insert, force_update, using)

    class Meta:
        verbose_name        = 'Link'
        verbose_name_plural = 'Links'
        ordering            = [ '-priority' , 'menu_entry' ]
        unique_together     = ( 'section' , 'slug' , 'hash' )

class ArticleQuerySet(models.query.QuerySet):
    def live(self):
        return self.filter(status=self.model.LIVE_STATUS)

class ArticleManagerAll(models.Manager):
    '''
    Hack to allow filtering for live objects in templates

    Unfortunately it only works if using a single site, if you use more than
    one site use the custom tags instead.
    '''
    use_for_related_fields = True

    def get_queryset(self):
        return ArticleQuerySet(self.model)

    def live(self, *args, **kwargs):
        return self.get_queryset().live(*args, **kwargs)

    def for_site(self, request):
        site = Site.objects.get_current(request)
        return self.live().filter(site=site)

class LiveArticleManager(models.Manager):
    '''Only live articles and the correct ones for the current site'''
    def get_queryset(self):
        qs = super(LiveArticleManager, self).get_queryset()
        return qs.filter(status=self.model.LIVE_STATUS)

    def for_site(self, request):
        site = Site.objects.get_current(request)
        return self.get_queryset().filter(site=site)

class ArticleEntry(BaseEntry):
    LIVE_STATUS      = 1
    DRAFT_STATUS     = 2
    PROTECTED_STATUS = 3
    STATUS_CHOICES = ( ( LIVE_STATUS      , 'Live'   )
                     , ( DRAFT_STATUS     , 'Draft'  )
                     , ( PROTECTED_STATUS , 'Hidden' )
                     )
    author       = models.ForeignKey(User,      related_name='articles')
    section      = models.ForeignKey(Section,   related_name='articles')
    site         = models.ManyToManyField(Site, related_name='articles')
    tags         = models.ManyToManyField(Tag,  related_name='articles')
    title        = models.CharField( max_length=200
                                   , help_text='Max 200 characters' )
    excerpt      = models.TextField(help_text='For search results (markdown)')
    body         = models.TextField(help_text='markdown')
    excerpt_html = models.TextField(editable=False, blank=True)
    body_html    = models.TextField(editable=False, blank=True)
    content_html = models.TextField(editable=False, blank=True)
    pub_date     = models.DateTimeField( blank=True
                                       , help_text='Added automatically' )
    mod_date     = models.DateTimeField(editable=False)
    status       = models.PositiveSmallIntegerField( choices=STATUS_CHOICES
                                                   , default=DRAFT_STATUS   )

    objects = ArticleManagerAll()
    live    = LiveArticleManager()

    def save(self, force_insert=False, force_update=False, using=None):
        self.body_html    = md2html(self.body)
        self.excerpt_html = md2html(self.excerpt)
        self.content_html = make_content(self.body)
        tz                = timezone(settings.TIME_ZONE)
        self.mod_date     = tz.localize(datetime.datetime.now())
        if not self.pk is None and self.pub_date is None:
            self.pub_date = self.mod_date
        if not self.LIVE_STATUS == self.status:
            self.featured = False
        if not self.featured:
            self.priority = 0
        if not self.hash:
            self.hash = sha1(self.body).hexdump()[:6]
        super(ArticleEntry, self).save(force_insert, force_update, using)

    def get_absolute_url(self):
        return reverse( 'sagittarius_article'
                      , kwargs={ 'article' : self.slug
                               , 'hash'    : self.hash
                               , 'section' : self.section.slug })

    class Meta:
        verbose_name        = 'Article'
        verbose_name_plural = 'Articles'
        ordering            = [ '-priority' , 'title' , '-mod_date' ]
        unique_together     = ( 'section' , 'slug' , 'hash' )

class LimitedSizeFileField(models.FileField):

    def __init__(self, *args, **kwargs):
        self.max_size = kwargs.pop('max_size', 0)
        self.max_help = kwargs.pop('max_help', 'unlimited')
        super(LimitedSizeFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(LimitedSizeFileField, self).clean(*args, **kwargs)
        if self.max_size and self.max_size < data.size:
            ftb = 'Sorry, file must be smaller than %s' % self.max_help
            raise forms.ValidationError(ftb, code='filetoobig')
        return data

class OverwriteStorage(FileSystemStorage):
    '''Storage hack that deletes a file if it is uploaded with the same name.

    This allows to update a file without giving it another name, but has its
    problems.  If the form do not check *ALL* database constraints carefully
    the file will be oerwritten but the rest of the data might not get saved!

    The file upload happens just after the save() method is called but before
    the commit to the database.  Be careful.
    '''

    def get_available_name(self, name):
        if self.exists(name):
            self.delete(name)
        return name

def upload_alt(inst, file):
    basename, ext = path.splitext(file)
    return 'sagittarius/%s/%s%s' % (inst.article.slug, slugify(basename), ext)

class AlternateEntry(models.Model):
    mname = models.CharField('Mnemonic Name', max_length=30)
    fname = LimitedSizeFileField( 'File'
                                , storage=OverwriteStorage()
                                , upload_to=upload_alt
                                , max_size=SAGITTARIUS_MAX_FILE_SIZE[0]
                                , max_help=SAGITTARIUS_MAX_FILE_SIZE[1]
                                )

    def get_absolute_url(self):
        return "%s%s" % (settings.MEDIA_URL, self.fname)

    def __unicode__(self):
        return self.article.slug + ' | ' + self.mname

    def delete(self, *args, **kwargs):
        '''Deletes the file when the record is deleted'''
        self.fname.delete()
        super(AlternateEntry, self).delete(*args, **kwargs)

    class Meta:
        abstract = True

class MediaEntry(AlternateEntry):
    article = models.ForeignKey(ArticleEntry, related_name='media')

    class Meta:
        verbose_name        = 'Article Media'
        verbose_name_plural = 'Article Media'
        ordering            = ['fname']
        # important! unique_together is *not* enforced by the admin
        unique_together     = ( ('article','fname') , ('article','mname') )

class FormatEntry(AlternateEntry):
    article = models.ForeignKey(ArticleEntry, related_name='formats')

    class Meta:
        verbose_name        = 'Alternate Format'
        verbose_name_plural = 'Alternate Formats'
        ordering            = ['fname']
        # important! unique_together is *not* enforced by the admin
        unique_together     = ( ('article','fname') , ('article','mname') )

