from django.utils.feedgenerator import Atom1Feed
from django.contrib.sites.models import Site
from django.contrib.syndication.feeds import Feed
from django.core.urlresolvers import reverse

from sagittarius.models import ArticleEntry

site = Site.objects.get_current()

class LatestArticlesFeed(Feed):
    author_name      = site.context.author
    copyright        = site.domain
    description      = "Latest articles posted on %s" % site.name
    feed_type        = Atom1Feed
    item_copyright   = site.domain
    item_author_name = site.context.author
    item_author_link = site.domain
    link             = reverse('sagittarius_feed')
    title            = "%s: Latest articles" % site.name

    def items(self):
        return ArticleEntry.live.all()[:12]

    def item_pubdate(self, item):
        return item.pub_date

    def item_guid(self, item):
        return site.domain + item.get_absolute_url()

    def item_categories(self, item):
        return [t.slug for t in item.tags.all()]

