from django.contrib import admin

from sagittarius import models

class SiteContextAdmin(admin.ModelAdmin):
    fields = ( 'site'
             , ( 'author' , 'copyright' )
             , 'logo'
             , 'motd'
             , 'news'
             , 'style'
             )
    list_display  = ( 'site' , 'author' , 'copyright' )
    list_filter   = ( 'site' ,)
    search_fields = ( 'site__domain' , 'author' , 'copyright' )
admin.site.register(models.SiteContext, SiteContextAdmin)

class SectionAdmin(admin.ModelAdmin):
    fields = ( 'title'
             , ( 'slug' , 'priority' )
             , 'description'
             )
    list_display  = ( 'title' , 'slug' , 'priority' )
    search_fields = ( 'title' , 'slug' , 'priority' )
admin.site.register(models.Section, SectionAdmin)

class TagAdmin(admin.ModelAdmin):
    search_fields = ('slug',)
admin.site.register(models.Tag, TagAdmin)

class LinkEntryAdmin(admin.ModelAdmin):
    fields = ( 'menu_entry'
             , ( 'slug'     , 'link'     )
             , ( 'featured' , 'priority' )
             , ( 'site'     , 'hash'     )
             , 'section'
             )
    list_display  = ( 'menu_entry' , 'section' , 'featured' , 'priority' )
    list_filter   = ( 'site' , 'section' , 'featured' )
    search_fields = ( 'menu_entry' , 'priority' )
admin.site.register(models.LinkEntry, LinkEntryAdmin)

class MediaEntryAdmin(admin.TabularInline):
    model = models.MediaEntry

class FormatEntryAdmin(admin.TabularInline):
    model = models.FormatEntry

class ArticleEntryAdmin(admin.ModelAdmin):
    fields = ( 'title'
             , ( 'menu_entry' , 'slug'     )
             , ( 'featured'   , 'priority' )
             , ( 'site'       , 'hash'     )
             , 'pub_date'
             , 'status'
             , 'section'
             , 'author'
             , 'excerpt'
             , 'body'
             , 'tags'
             )
    list_display  = ( 'title' , 'section' , 'featured' , 'priority' )
    list_filter   = ( 'site'  , 'section' , 'featured' )
    search_fields = ( 'title' , 'excerpt' , 'body' )
    inlines       = ( MediaEntryAdmin , FormatEntryAdmin )
    filter_horizontal = ('tags',)
admin.site.register(models.ArticleEntry, ArticleEntryAdmin)

