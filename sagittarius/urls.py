from django.conf.urls import include, url
from sagittarius.views import SectionView , LinkView , TagView  \
                            , SearchView  , FeedView \
                            , ArticleView , NewsView

urlpatterns = \
[ url( r'^section/(?P<section>[\w-]+)/$'
     , SectionView.as_view(), name='sagittarius_section' )
, url( r'^link/(?P<hash>[0-9a-f]+)/(?P<link>[\w-]+)/$'
     , LinkView.as_view(), name='sagittarius_link' )
, url( r'^tag/(?P<tag>[\w-]+)/$'
     , TagView.as_view(), name='sagittarius_tag' )
, url( r'^search/'
     , SearchView.as_view(), name='sagittarius_search' )
, url( r'^atom/$'
     , FeedView.as_view(), name='sagittarius_atom' )
, url( r'^feed/'  # TODO: use djnago.syndication (page 142)
     , FeedView.as_view(), name='sagittarius_feed' )
, url( r'^(?P<section>[\w-]+)/(?P<hash>[0-9a-f]+)/(?P<article>[\w-]+)/$'
     , ArticleView.as_view(), name='sagittarius_article' )
, url( r'^$'
     , NewsView.as_view(), name='sagittarius_news' )
]

