from django.contrib.sites.models import Site
from sagittarius.models import Section

def copyright_processor(request):
    return { 'site' : Site.objects.get_current(request) }

def navmenu_processor(request):
    menu = Section.objects.all()
    return { 'menu_sections' : menu }

