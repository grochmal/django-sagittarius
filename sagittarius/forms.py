from django import forms

class SearchForm(forms.Form):
    QUERY_WIDGET = forms.TextInput(attrs={ 'size'        : '40'
                                         , 'placeholder' : 'type your query' })
    query        = forms.CharField(max_length=60, widget=QUERY_WIDGET)
    search_title = forms.BooleanField( required=False, initial=True
                                     , label='Search Title'         )
    search_body  = forms.BooleanField( required=False, initial=True
                                     , label='Search Content'       )
    search_tag   = forms.BooleanField( required=False, initial=False
                                     , label='Search Tags'           )
    search_case  = forms.BooleanField( required=False, initial=False
                                     , label='Case Sensitive Search' )

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.label_suffix = ''
        self.fields['query'].error_messages = { 'required'
                                              : 'You must give a query' }

    def clean_query(self):
        query = self.cleaned_data['query'].strip()
        if not query:
            raise forms.ValidationError('You must input a query')
        return query

