from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()

class AbsoluteUrlNode(template.Node):
    def __init__(self, urlobj=None, urlquoted=None):
        self.quoted = urlquoted
        self.urlobj = None
        if urlobj: self.urlobj = template.Variable(urlobj)

    def render(self, context):
        rel_url = None
        request = context.get('request', None)
        if self.quoted:
            try: rel_url = reverse(self.quoted[1:-1])
            except NoReverseMatch: pass
        elif self.urlobj:
            try: rel_url = self.urlobj.resolve(context)
            except template.VariableDoesNotExist: pass
        if rel_url and request:
            return request.build_absolute_uri(rel_url)
        return ''

def do_absolute_url(parser, token):
    try:
        name, urlobj = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError( "%r tag accepts one argument"
                                          % token.contents.split()[0]     )
    if urlobj[0] in ('"', "'") and urlobj[0] == urlobj[-1]:  # is quoted
        return AbsoluteUrlNode(urlquoted=urlobj)
    return AbsoluteUrlNode(urlobj=urlobj)

register.tag('absolute_url', do_absolute_url)

def paginate_this(context):
    return { 'page_obj' : context['page_obj'] }
register.inclusion_tag( 'sagittarius/include/paginate_this.html'
                      , takes_context=True               )(paginate_this)

def paginate_form(context):
    return { 'page_obj'  : context['article_list']
           , 'get_query' : context['get_query']
           }
register.inclusion_tag( 'sagittarius/include/paginate_form.html'
                      , takes_context=True               )(paginate_form)

def list_articles(context):
    return { 'article_list' : context['article_list'] }
register.inclusion_tag( 'sagittarius/include/list_articles.html'
                      , takes_context=True               )(list_articles)

def site_entries(context, section_entries):
    request = context.get('request', None)
    if request : entries = section_entries.for_site(request)
    else       : entries = section_entries.live()
    return entries
register.assignment_tag(takes_context=True)(site_entries)

