#!/usr/bin/env python

try:
    from setuptools import setup
except ImportError:
    from distutils import setup
import os
import sagittarius

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

# Full list of classifiers can be found here:
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
CLS = \
 [ 'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)'
 , 'Development Status :: 3 - Alpha'
 , 'Environment :: Web Environment'
 , 'Framework :: Django'
 , 'Intended Audience :: Education'
 , 'Operating System :: OS Independent'
 , 'Programming Language :: Python'
 , 'Topic :: Text Processing :: Markup :: HTML'
 ]

REQS = [ 'django >= 1.8'
       , 'PyYAML >= 3.11'
       , 'pytz >= 2015.4'
       , 'Markdown >= 2.6.2'
       ]

setup( name             = sagittarius.pkgname
     , description      = sagittarius.__description__
     , version          = sagittarius.__version__
     , author           = sagittarius.__author__
     , author_email     = sagittarius.__author_email__
     , license          = sagittarius.__license__
     , url              = sagittarius.__url__
     , long_description = read('README')
     , packages         = [ 'sagittarius' ]
     , classifiers      = CLS
     , install_requires = REQS
     )

