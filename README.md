django-sagittarius
==================

a publishing engine designed to be text browser friendly
--------------------------------------------------------

django-sagittarius is designed with text browsers in mind, e.g lynx, elinks or
w3m, whilst still looking modern on graphical browsers.  To allow for this the
use of JavaScript is used only for members that are visible on graphical
browsers (e.g. images).  Also, the use of CSS is limited to presentation only,
not allowing CSS to add or remove content for a "nicer" visualisation.

The package is also django.contib.sites aware, it is possible (or even desired)
that articles are published on specific sites and on those sites only.

Copying
-------

Copyright (C) 2015 Michal Grochmal

This file is part of the django-sagittarius package.

django-sagittarius is free software; you can redistribute and/or modify all or
part of the code under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

django-sagittarius is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

The COPYING file in the root directory of the project contains a copy of the
GNU General Public License.  If you cannot find this file, see
<http://www.gnu.org/licenses/>.

TODO
----

* Add the `markdown` format as an alternate format of every article.

* Remove the "Contents" div if there is not index for the article, this will
  not be needed anymore once the `markdown` format is present on every article.

* Add a `sitemap` and upload it into google's indexing engine.

